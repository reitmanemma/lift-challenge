'use client'
import styled from "styled-components";
import React from 'react';


interface ButtonProps {
  value: number;
  href?: string;
  onClick: (value) => void;
  isDisabled?: boolean;
}

export function Button({value, href, onClick, isDisabled}: ButtonProps) {
  return (
    <StyledButton onClick={() => onClick(value)} disabled={isDisabled}>
      <StyledLink href={href}>
        {value}
      </StyledLink>
    </StyledButton>
  )
}

const StyledButton = styled.button`
background-image: linear-gradient(to right, rgb(198, 177, 116), white);
padding: 1rem;
border-radius: 50%;
width: 2.75rem;
height: 2.75rem;
border-color: black;
border-width: 0.2rem;
display: flex;
align-items: center;
justify-content: center;
cursor: pointer;
&:hover {
  background-image: linear-gradient(to right, rgb(140, 123, 78), white);
}
&:disabled {
  cursor: default;
  opacity: 1;
  background: white;
}
`;

const StyledLink = styled.a`
color: rgb(117, 91, 51);
font-size: 1rem;
font-family: Georgia;
text-decoration: none;
`;



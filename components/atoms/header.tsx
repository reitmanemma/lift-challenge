'use client'
import styled from "styled-components";
import React from 'react';


interface HeaderProps {
    value: string;
}

export function Header({value}: HeaderProps) {
  return (
    <StyledHeader>{value}</StyledHeader>
  )
}

const StyledHeader = styled.h1`
  color: rgb(43, 37, 14);
  font-size: 2rem;
  font-family: Zapfino;
  text-align: center;
  margin: 0rem;
  padding-top: 8rem;
`;


'use client'
import React from "react";
import styled from "styled-components";

interface ScreenProps {
  value?: number | number[] | string;
  isArrow?: boolean;
  src?: string;
  isNumbers?: boolean;
}

export function Screen({value, isArrow, src, isNumbers}: ScreenProps) {
  return (
  <Container $isNumbers>
    <Display>
      { isArrow ? <Arrow src={src} alt={src}/> : <Value>{value}</Value>}
    </Display>
  </Container>
  )
}

const Arrow = styled.img`
width: 8rem;
`;

const Container = styled.div<{ $isNumbers?: boolean; }>`
width: ${p => p.$isNumbers ? "9rem" : ""};
height: fit-content;
background: white;
align-self: center;
`;

const Display = styled.div`
border: black;
border-width: 0.5rem;
padding: 1rem;
display: flex;
flex-wrap: wrap;
`;

const Value = styled.p`
color: rgb(117, 91, 51);
font-size: 1rem;
font-family: Georgia;
line-height: 0rem;
`;




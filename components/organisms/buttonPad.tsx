'use client'
import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import { Button } from "../atoms/button";
import { Screen } from "../molecules/screen";

interface ButtonPadProps {
    floors: number[];
}

export function ButtonPad({floors}: ButtonPadProps) {
    const [chosenFloors, setChosenFloors] = useState([]);
    const [currentFloor, setCurrentFloor] = useState(0);
    const [nextFloor, setNextFloor] = useState(0);
    const [disabled, setDisabled] = useState(false);
    const [liftStatus, setLiftStatus] = useState("Lift at floor 0")
    const [displayChange, setDisplayChange] = useState([])

    useEffect(() => {
        (currentFloor === 0 && nextFloor === 0 && chosenFloors.length === 0) || 
        (nextFloor !== currentFloor) || 
        (currentFloor === nextFloor) ? 
        setDisabled(true) 
        : setDisabled(false);

        if (0 < chosenFloors.length && chosenFloors.length < 5) {
            setLiftStatus("Lift en route")
        }
        if (chosenFloors.length === 6) {
            const queuedFloor = chosenFloors.shift()
            const floorsRemaining = chosenFloors.filter(number => number != queuedFloor)
            setDisplayChange(floorsRemaining) 
            setChosenFloors(floorsRemaining)
            setLiftStatus(`Lift at floor ${queuedFloor}`)
        } else {
            setDisplayChange(chosenFloors)
        }
        return () => {
            setDisabled(false);
        };
    },[currentFloor, setCurrentFloor, chosenFloors, nextFloor, setDisplayChange, setChosenFloors, setLiftStatus])

    const displayImage = () => {
        if (currentFloor < nextFloor) {
            return "/images/up_arrow.jpg"
        }
        if (currentFloor > nextFloor) {
            return "/images/down_arrow.jpg"
        }
        else {
            return "/images/choose_a_floor.jpg"
        }
    }
    return (
        <OuterPad>
            <LeftContainer>
                <ScreenContainer>
                    <Screen value={liftStatus} />
                    <Screen value={displayChange.join(", ")} isNumbers />
                    <Container>
                        <Screen value={chosenFloors} isArrow src={(displayImage())} />
                    </Container>
                </ScreenContainer>
            </LeftContainer>
            <RightContainer>
                {
                    floors.map((number) => {
                        return <Button key={number} value={number} isDisabled={number === nextFloor ? disabled : null}
                        onClick={(value) => {
                            setChosenFloors([...chosenFloors, value])
                            setNextFloor(value)
                            setCurrentFloor(nextFloor)
                        }}
                        />
                    })
                }
            </RightContainer>
        </OuterPad>
    )
}

const Container = styled.div`
height: 8rem;
`;

const ScreenContainer = styled.div`
display: flex;
flex-direction: column;
gap: 1rem;
padding-top: 2rem;
margin: 0 auto;
`;

const LeftContainer = styled.div`
background-image: linear-gradient(to right, rgb(72, 64, 57), rgb(134, 113, 72));
width: 75%;
display: flex;
`;

const RightContainer = styled.div`
background-image: linear-gradient(to right, rgb(193, 171, 81), rgb(239, 232, 206));
width: 25%;
padding: 1rem;
display: flex;
flex-direction: column;
align-items: center;
gap: 0.25rem;
`;

const OuterPad = styled.div`
width: 16rem;
display: flex;
background: red;
`;




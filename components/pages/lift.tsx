'use client'
import React from 'react';
import styled from "styled-components";
import { ButtonPad } from '../organisms/buttonPad';
import { lifts } from '../../utils/consts';

interface LiftProps {
    allServicedFloors: typeof lifts;
}

export function Lift({allServicedFloors}: LiftProps) {

  return (
    <Container>
        {
            Object.values(allServicedFloors || {}).map((floors) => {
                return Object.values(floors).map((floor) => {
                    return <ButtonPad floors={floor} key="floors"/>
                })
            })
        }
    </Container>
  )
}

const Container = styled.div`
display: flex;
gap: 1rem;
justify-content: center;
`;



'use client'
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import React from "react";
import styled from "styled-components";
import { Lift } from '../components/pages/lift';
import { lifts } from '../utils/consts';
import { Header } from '../components/atoms/header';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>My Digitalis Lift App</title>
        <link rel="icon" href="/favicon.jpg" />
      </Head>
      <main>
        <Container>
          <Header value="Hotel Digitalis" />
          <Lift allServicedFloors={lifts}/>
        </Container>
      </main>
    </div>
  );
}

const Container = styled.div`
background-image: url("/lift_border.jpg");
background-size: cover;
background-repeat: no-repeat;
height: 60rem;
width: 50rem; 
margin: 0 auto;
`;

